package wk6;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Driver {
    public static final int SIZE = 5000000;

    public static void main(String[] args) {
        List<Integer> numbers = new ArrayList<>();
        for(int i=0; i<SIZE; i++) {
            numbers.add((int)(Math.random()*SIZE));
        }
        Collections.sort(numbers);
        System.out.println("done");
        //System.out.println(numbers);
        System.out.println(binarySearch(numbers, 2));
    }

    public static <T> boolean binarySearch(List<? extends Comparable<T>> list, T target) {
        return binarySearch(list, target, 0, list.size());
    }

    public static <T> boolean binarySearch(List<? extends Comparable<? super T>> list, T target, int start, int end) {
        boolean foundIt = false;
        if(start<end) {
            int middle = (end+start)/2;
            int comparison = list.get(middle).compareTo(target);
            if(comparison==0) {
                foundIt = true;
            } else if(comparison<0) {
                foundIt = binarySearch(list, target, middle + 1, end);
            } else if(comparison>0) {
                foundIt = binarySearch(list, target, start, middle);
            }
        }
        return foundIt;
    }


}

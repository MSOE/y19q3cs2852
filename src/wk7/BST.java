package wk7;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

public class BST<E extends Comparable<? super E>> implements Set<E> {

    private Node<E> root;

    private static class Node<E extends Comparable<? super E>> {
        private E value;
        private Node<E> lKid;
        private Node<E> rKid;

        private Node(E value) {
            this(value, null, null);
        }

        private Node(E value, Node<E> left, Node<E> right) {
            this.value = value;
            lKid = left;
            rKid = right;
        }
    }

    private Node<E> leftRotate(Node<E> subroot) {
        Node<E> A = subroot;
        Node<E> B = A.rKid;
        Node<E> x = B.lKid;
        A.rKid = x;
        B.lKid = A;
        return B;
    }

    /**
     * Returns the number of elements in this set (its cardinality).  If this
     * set contains more than {@code Integer.MAX_VALUE} elements, returns
     * {@code Integer.MAX_VALUE}.
     *
     * @return the number of elements in this set (its cardinality)
     */
    @Override
    public int size() {
        return size(root);
    }

    private int size(Node<E> subroot) {
        int size = 0;
        if(subroot!=null) {
            size = 1 + size(subroot.lKid) + size(subroot.rKid);
        }
        return size;
    }

    /**
     * Returns {@code true} if this set contains no elements.
     *
     * @return {@code true} if this set contains no elements
     */
    @Override
    public boolean isEmpty() {
        return root==null;
    }

    /**
     * Returns {@code true} if this set contains the specified element.
     * More formally, returns {@code true} if and only if this set
     * contains an element {@code e} such that
     * {@code Objects.equals(o, e)}.
     *
     * @param target element whose presence in this set is to be tested
     * @return {@code true} if this set contains the specified element
     * @throws ClassCastException   if the type of the specified element
     *                              is incompatible with this set
     *                              (<a href="Collection.html#optional-restrictions">optional</a>)
     * @throws NullPointerException if the specified element is null and this
     *                              set does not permit null elements
     *                              (<a href="Collection.html#optional-restrictions">optional</a>)
     */
    @Override
    public boolean contains(Object target) {
        try {
            E value = (E)target;
        } catch(ClassCastException e) {
            return false;
        }
        return contains((E)target, root);
    }

    private boolean contains(E target, Node<E> subroot) {
        if(subroot==null) {
            return false;
        }

        boolean foundIt;
        int comparison = subroot.value.compareTo(target);
        if(comparison==0) {
            foundIt = true;
        } else if(comparison<0) {
            foundIt = contains(target, subroot.rKid);
        } else {
            foundIt = contains(target, subroot.lKid);
        }
        return foundIt;
    }

    /**
     * Removes all of the elements from this set (optional operation).
     * The set will be empty after this call returns.
     *
     * @throws UnsupportedOperationException if the {@code clear} method
     *                                       is not supported by this set
     */
    @Override
    public void clear() {
        root = null;
    }

    /**
     * Adds the specified element to this set if it is not already present
     * (optional operation).  More formally, adds the specified element
     * {@code e} to this set if the set contains no element {@code e2}
     * such that
     * {@code Objects.equals(e, e2)}.
     * If this set already contains the element, the call leaves the set
     * unchanged and returns {@code false}.  In combination with the
     * restriction on constructors, this ensures that sets never contain
     * duplicate elements.
     *
     * <p>The stipulation above does not imply that sets must accept all
     * elements; sets may refuse to add any particular element, including
     * {@code null}, and throw an exception, as described in the
     * specification for {@link Collection#add Collection.add}.
     * Individual set implementations should clearly document any
     * restrictions on the elements that they may contain.
     *
     * @param element element to be added to this set
     * @return {@code true} if this set did not already contain the specified
     * element
     * @throws UnsupportedOperationException if the {@code add} operation
     *                                       is not supported by this set
     * @throws ClassCastException            if the class of the specified element
     *                                       prevents it from being added to this set
     * @throws NullPointerException          if the specified element is null and this
     *                                       set does not permit null elements
     * @throws IllegalArgumentException      if some property of the specified element
     *                                       prevents it from being added to this set
     */
    @Override
    public boolean add(E element) {
        if(element==null) {
            throw new NullPointerException("Set does not support null");
        }
        boolean added = false;
        if(root==null) {
            root = new Node<>(element);
            added = true;
        } else {
            added = add(element, root);
        }
        return added;
    }

    private boolean add(E element, Node<E> subroot) {
        boolean added = false;
        int comparison = subroot.value.compareTo(element);
        if(comparison<0) {
            if(subroot.rKid==null) {
                subroot.rKid = new Node<>(element);
                added = true;
            } else {
                added = add(element, subroot.rKid);
            }
        } else if(comparison>0) {
            if(subroot.lKid==null) {
                subroot.lKid = new Node<>(element);
                added = true;
            } else {
                added = add(element, subroot.lKid);
            }
        }
        return added;
    }

    private int height() {
        return height(root);
    }

    private int height(Node<E> subroot) {
        int height = 0;
        if(subroot!=null) {
            height = 1 + Math.max(height(subroot.lKid), height(subroot.rKid));
        }
        return height;
    }

    /**
     * Removes the specified element from this set if it is present
     * (optional operation).  More formally, removes an element {@code e}
     * such that
     * {@code Objects.equals(o, e)}, if
     * this set contains such an element.  Returns {@code true} if this set
     * contained the element (or equivalently, if this set changed as a
     * result of the call).  (This set will not contain the element once the
     * call returns.)
     *
     * @param o object to be removed from this set, if present
     * @return {@code true} if this set contained the specified element
     * @throws ClassCastException            if the type of the specified element
     *                                       is incompatible with this set
     *                                       (<a href="Collection.html#optional-restrictions">optional</a>)
     * @throws NullPointerException          if the specified element is null and this
     *                                       set does not permit null elements
     *                                       (<a href="Collection.html#optional-restrictions">optional</a>)
     * @throws UnsupportedOperationException if the {@code remove} operation
     *                                       is not supported by this set
     */
    @Override
    public boolean remove(Object o) {
        return false;
    }

    /**
     * Returns {@code true} if this set contains all of the elements of the
     * specified collection.  If the specified collection is also a set, this
     * method returns {@code true} if it is a <i>subset</i> of this set.
     *
     * @param c collection to be checked for containment in this set
     * @return {@code true} if this set contains all of the elements of the
     * specified collection
     * @throws ClassCastException   if the types of one or more elements
     *                              in the specified collection are incompatible with this
     *                              set
     *                              (<a href="Collection.html#optional-restrictions">optional</a>)
     * @throws NullPointerException if the specified collection contains one
     *                              or more null elements and this set does not permit null
     *                              elements
     *                              (<a href="Collection.html#optional-restrictions">optional</a>),
     *                              or if the specified collection is null
     * @see #contains(Object)
     */
    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    /**
     * Adds all of the elements in the specified collection to this set if
     * they're not already present (optional operation).  If the specified
     * collection is also a set, the {@code addAll} operation effectively
     * modifies this set so that its value is the <i>union</i> of the two
     * sets.  The behavior of this operation is undefined if the specified
     * collection is modified while the operation is in progress.
     *
     * @param c collection containing elements to be added to this set
     * @return {@code true} if this set changed as a result of the call
     * @throws UnsupportedOperationException if the {@code addAll} operation
     *                                       is not supported by this set
     * @throws ClassCastException            if the class of an element of the
     *                                       specified collection prevents it from being added to this set
     * @throws NullPointerException          if the specified collection contains one
     *                                       or more null elements and this set does not permit null
     *                                       elements, or if the specified collection is null
     * @throws IllegalArgumentException      if some property of an element of the
     *                                       specified collection prevents it from being added to this set
     * @see #add(E)
     */
    @Override
    public boolean addAll(Collection<? extends E> c) {
        return false;
    }

    /**
     * Removes from this set all of its elements that are contained in the
     * specified collection (optional operation).  If the specified
     * collection is also a set, this operation effectively modifies this
     * set so that its value is the <i>asymmetric set difference</i> of
     * the two sets.
     *
     * @param c collection containing elements to be removed from this set
     * @return {@code true} if this set changed as a result of the call
     * @throws UnsupportedOperationException if the {@code removeAll} operation
     *                                       is not supported by this set
     * @throws ClassCastException            if the class of an element of this set
     *                                       is incompatible with the specified collection
     *                                       (<a href="Collection.html#optional-restrictions">optional</a>)
     * @throws NullPointerException          if this set contains a null element and the
     *                                       specified collection does not permit null elements
     *                                       (<a href="Collection.html#optional-restrictions">optional</a>),
     *                                       or if the specified collection is null
     * @see #remove(Object)
     * @see #contains(Object)
     */
    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    /**
     * Retains only the elements in this set that are contained in the
     * specified collection (optional operation).  In other words, removes
     * from this set all of its elements that are not contained in the
     * specified collection.  If the specified collection is also a set, this
     * operation effectively modifies this set so that its value is the
     * <i>intersection</i> of the two sets.
     *
     * @param c collection containing elements to be retained in this set
     * @return {@code true} if this set changed as a result of the call
     * @throws UnsupportedOperationException if the {@code retainAll} operation
     *                                       is not supported by this set
     * @throws ClassCastException            if the class of an element of this set
     *                                       is incompatible with the specified collection
     *                                       (<a href="Collection.html#optional-restrictions">optional</a>)
     * @throws NullPointerException          if this set contains a null element and the
     *                                       specified collection does not permit null elements
     *                                       (<a href="Collection.html#optional-restrictions">optional</a>),
     *                                       or if the specified collection is null
     * @see #remove(Object)
     */
    @Override
    public boolean retainAll(Collection<?> c) {
        throw new UnsupportedOperationException("no implemented");
    }

    /**
     * Returns an iterator over the elements in this set.  The elements are
     * returned in no particular order (unless this set is an instance of some
     * class that provides a guarantee).
     *
     * @return an iterator over the elements in this set
     */
    @Override
    public Iterator<E> iterator() {
        throw new UnsupportedOperationException("no implemented");
    }

    /**
     * Returns an array containing all of the elements in this set.
     * If this set makes any guarantees as to what order its elements
     * are returned by its iterator, this method must return the
     * elements in the same order.
     *
     * <p>The returned array will be "safe" in that no references to it
     * are maintained by this set.  (In other words, this method must
     * allocate a new array even if this set is backed by an array).
     * The caller is thus free to modify the returned array.
     *
     * <p>This method acts as bridge between array-based and collection-based
     * APIs.
     *
     * @return an array containing all the elements in this set
     */
    @Override
    public Object[] toArray() {
        throw new UnsupportedOperationException("no implemented");
    }

    /**
     * Returns an array containing all of the elements in this set; the
     * runtime type of the returned array is that of the specified array.
     * If the set fits in the specified array, it is returned therein.
     * Otherwise, a new array is allocated with the runtime type of the
     * specified array and the size of this set.
     *
     * <p>If this set fits in the specified array with room to spare
     * (i.e., the array has more elements than this set), the element in
     * the array immediately following the end of the set is set to
     * {@code null}.  (This is useful in determining the length of this
     * set <i>only</i> if the caller knows that this set does not contain
     * any null elements.)
     *
     * <p>If this set makes any guarantees as to what order its elements
     * are returned by its iterator, this method must return the elements
     * in the same order.
     *
     * <p>Like the {@link #toArray()} method, this method acts as bridge between
     * array-based and collection-based APIs.  Further, this method allows
     * precise control over the runtime type of the output array, and may,
     * under certain circumstances, be used to save allocation costs.
     *
     * <p>Suppose {@code x} is a set known to contain only strings.
     * The following code can be used to dump the set into a newly allocated
     * array of {@code String}:
     *
     * <pre>
     *     String[] y = x.toArray(new String[0]);</pre>
     * <p>
     * Note that {@code toArray(new Object[0])} is identical in function to
     * {@code toArray()}.
     *
     * @param a the array into which the elements of this set are to be
     *          stored, if it is big enough; otherwise, a new array of the same
     *          runtime type is allocated for this purpose.
     * @return an array containing all the elements in this set
     * @throws ArrayStoreException  if the runtime type of the specified array
     *                              is not a supertype of the runtime type of every element in this
     *                              set
     * @throws NullPointerException if the specified array is null
     */
    @Override
    public <T> T[] toArray(T[] a) {
        throw new UnsupportedOperationException("no implemented");
    }

    private int width() {
        final int nodeWidth = 4;
        return (int)Math.pow(2, height())/2 * nodeWidth;
    }

    // Based off of https://rosettacode.org/wiki/Visualize_a_tree
    public void display() {
        final int height = height();
        final int width = width()+1;

        int length = width * height * 2 + 2;
        StringBuilder sb = new StringBuilder(length);
        // Insert space and newline characters
        for (int i = 1; i <= length; i++) {
            sb.append(i < length - 2 && i % width == 0 ? "\n" : ' ');
        }

        displayR(sb, width / 2, 1, width / 4, width, root, " ");
        System.out.println(sb);
    }

    private void displayR(StringBuilder sb, int c, int r, int d, int width, Node<E> subroot,
                          String edge) {
        if (subroot != null) {
            displayR(sb, c - d, r + 2, d / 2, width, subroot.lKid, " /");

            String s = subroot.value.toString();
            int idx1 = r * width + c - (s.length() + 1) / 2;
            int idx2 = idx1 + s.length();
            int idx3 = idx1 - width;
            if (idx2 < sb.length())
                sb.replace(idx1, idx2, s).replace(idx3, idx3 + 2, edge);

            displayR(sb, c + d, r + 2, d / 2, width, subroot.rKid, "\\ ");
        }
    }

    public void preOrder(Consumer<E> consumer) {
        preOrder(root, consumer);
    }

    private void preOrder(Node<E> toorbus, Consumer<E> consumer) {
        if(toorbus!=null) {
            consumer.accept(toorbus.value);
            preOrder(toorbus.lKid, consumer);
            preOrder(toorbus.rKid, consumer);
        }
    }

    public void inOrder(Consumer<E> consumer) {
        inOrder(root, consumer);
    }

    private void inOrder(Node<E> toorbus, Consumer<E> consumer) {
        if(toorbus!=null) {
            inOrder(toorbus.lKid, consumer);
            consumer.accept(toorbus.value);
            inOrder(toorbus.rKid, consumer);
        }
    }

    public void postOrder(Consumer<E> consumer) {
        postOrder(root, consumer);
    }

    private void postOrder(Node<E> toorbus, Consumer<E> consumer) {
        if(toorbus!=null) {
            postOrder(toorbus.lKid, consumer);
            postOrder(toorbus.rKid, consumer);
            consumer.accept(toorbus.value);
        }
    }

    public static void main(String[] args) {
        //final int[] elements = {};
        final int[] elements = {50, 25, 13, 38, 75};
        //final int[] elements = {50, 25, 13, 7, 3, 11, 38, 45, 48, 20, 23, 17, 75};
        BST<Integer> bst = new BST<>();
        for(int number : elements) {
            bst.add(number);
        }
        System.out.println(bst.contains(20));
        bst.display();
        bst.preOrder(e -> System.out.print(e + ", "));
        System.out.println();
        bst.inOrder(e -> System.out.print(e + ", "));
        System.out.println();
        bst.postOrder(e -> System.out.print(e + ", "));
        System.out.println();
        System.out.println(bst.twoString());
        BST<Integer> copy = bst.copy();
        bst.add(8888);
        copy.display();
        System.out.println(bst);
    }

    public String twoString() {
        StringBuilder builder = new StringBuilder();
        inOrder(e -> builder.append(e + ", "));
        return "[" + builder.toString().substring(0, Math.max(0, builder.length()-2)) + "]";
    }

    public BST<E> copy() {
        BST<E> copy = new BST<>();
        preOrder(e -> copy.add(e));
        return copy;
    }

    public void preOrderTraversal(BiConsumer<E, Integer> consumer) {
        preOrderTraversal(root, 1, consumer);
    }

    private void preOrderTraversal(Node<E> toorbus, int depth, BiConsumer<E, Integer> consumer) {
        if(toorbus==null) {
            consumer.accept(null, depth);
        } else {
            consumer.accept(toorbus.value, depth);
            preOrderTraversal(toorbus.lKid, depth+1, consumer);
            preOrderTraversal(toorbus.rKid, depth+1, consumer);
        }
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        preOrderTraversal((e, d) -> {
            for(int i=1; i<d; i++) {
                builder.append(" ");
            }
            builder.append(e);
            builder.append("\n");
        });
        return builder.toString();
    }
}

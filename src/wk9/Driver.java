package wk9;

import javafx.scene.paint.Color;

public class Driver {
    public static void main(String[] args) {
        Shape[] shapes = new Shape[3];
        for(int i=0; i<shapes.length; i++) {
            shapes[i] = new Shape((int)(Math.random()*5),(int)(Math.random()*5),
                    Color.color(Math.random(),Math.random(), Math.random()));
        }
        System.out.println(shapes);
        Shape[] shapes2 = new Shape[shapes.length];
        {
            int i = 0;
            for (Shape shape : shapes) {
                shapes2[i++] = shape.clone();
            }
        }
        Shape[] shapes3 = new Shape[shapes.length];
        {
            int i = 0;
            for (Shape shape : shapes) {
                shapes3[i++] = new Shape(shape);
            }
        }
        System.out.println(shapes2);
        System.out.println(shapes3);
    }
}

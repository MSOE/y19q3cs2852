package wk5;

public class Driver {
    private int answer = 0;
    public static void main(String[] args) {
        Driver andrew = new Driver();
        System.out.println(andrew.sumToN(14));
        System.out.println(arrayToString(args));
    }

    public static String arrayToString(String[] words) {
        StringBuilder builder = new StringBuilder("[");
        for(String word : words) {
            builder.append(word + ", ");
        }
        builder.deleteCharAt(builder.length()-1);
        builder.deleteCharAt(builder.length()-1);
        builder.append("]");
        return builder.toString();
    }

    // fib(n) = fib(n-1) + fib(n-2)
    public int fib(int n) {
        if(n<0) {
            throw new IllegalArgumentException("What!");
        }
        int answer = 1;
        if(n>1) {
            answer = fib(n-1) + fib(n-2);
        }
        return answer;
    }

    public int sumToN(int n) {
        if(n<0) {
            throw new IllegalArgumentException("What!");
        }
        answer = 0;
        if(n>0) {
            answer = sumToN(n - 1) + n;
        }
        return answer;
    }

}

package wk2;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Objects;

public class LinkedList<E> implements List<E> {

    private Node<E> head;

    private static class Node<E> {
        private Node<E> next;
        private E value;

        private Node(E element) {
            this(element, null);
        }

        private Node(E element, Node<E> next) {
            value = element;
            this.next = next;
        }
    }

    public LinkedList() {
        head = null;
    }

    @Override
    public int size() {
        int count = 0;
        Node<E> walker = head;
        while(walker!=null) {
            walker = walker.next;
            ++count;
        }
        return count;
    }

    @Override
    public boolean isEmpty() {
        return head==null;
    }

    @Override
    public boolean contains(Object target) {
        return indexOf(target)!=-1;
    }

    @Override
    public boolean add(E e) {
        Node<E> newNode = new Node<>(e);
        if(isEmpty()) {
            head = newNode;
        } else {
            Node<E> end = getNode(size() - 1);
            end.next = newNode;
        }
        return true;
    }

    private Node<E> getNode(int index) {
        if(index<0 || index>=size()) {
            throw new IndexOutOfBoundsException("Index " + index + " on LinkedList of size " + size());
        }
        Node<E> walker = head;
        while(index>0) {
            walker = walker.next;
            --index;
        }
        return walker;
    }

    @Override
    public void clear() {
        head = null;
    }

    @Override
    public E get(int index) {
        return null;
    }

    @Override
    public E set(int index, E element) {
        return null;
    }

    @Override
    public void add(int index, E element) {

    }

    @Override
    public boolean remove(Object target) {
        if(isEmpty()) {
            return false;
        }
        boolean changed = false;
        if(Objects.equals(head.value, target)) {
            head = head.next;
            changed = true;
        } else {
            Node<E> walker = head;
            while(!changed && walker.next!=null) {
                if (Objects.equals(target, walker.next.value)) {
                    walker.next = walker.next.next;
                    changed = true;
                } else {
                    walker = walker.next;
                }
            }
        }
        return changed;
    }

    @Override
    public E remove(int index) {
        return null;
    }

    @Override
    public int indexOf(Object target) {
        return 0;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public Iterator<E> iterator() {
        throw new UnsupportedOperationException("too lazy");
    }

    @Override
    public <T> T[] toArray(T[] a) {
        throw new UnsupportedOperationException("too lazy");
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        throw new UnsupportedOperationException("too lazy");
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        throw new UnsupportedOperationException("too lazy");
    }

    @Override
    public boolean addAll(int index, Collection<? extends E> c) {
        throw new UnsupportedOperationException("too lazy");
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        throw new UnsupportedOperationException("too lazy");
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        throw new UnsupportedOperationException("too lazy");
    }

    @Override
    public int lastIndexOf(Object target) {
        throw new UnsupportedOperationException("too lazy");
    }

    @Override
    public ListIterator<E> listIterator() {
        throw new UnsupportedOperationException("too lazy");
    }

    @Override
    public ListIterator<E> listIterator(int index) {
        throw new UnsupportedOperationException("too lazy");
    }

    @Override
    public List<E> subList(int fromIndex, int toIndex) {
        throw new UnsupportedOperationException("too lazy");
    }
}

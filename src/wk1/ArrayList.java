package wk1;

import edu.msoe.cs2852.Emoji;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.RandomAccess;

public class ArrayList<E> implements List<E>, RandomAccess {
    public static void main(String[] args) {
        List<Character> list = new java.util.ArrayList<>();
        list.add(Emoji.getCardSuit());
        list.add(Emoji.getCardSuit());
        list.add(Emoji.getCardSuit());
        for(int i=0; i<list.size(); i++) {
            System.out.println(list.get(i));
        }
        {
            Iterator<Character> itr = list.iterator();
            while (itr.hasNext()) {
                Character suit = itr.next();
                System.out.println(suit);
            }
        }
        for(Character suit : list) {
            System.out.println(suit);
        }
    }

    private E[] array;

    public ArrayList() {
        clear();
    }

    @Override
    public int size() {
        return array.length;
    }

    @Override
    public boolean isEmpty() {
        return size()==0;
    }

    @Override
    public boolean add(E element) {
        E[] newArray = (E[])new Object[size()+1];
        System.arraycopy(array, 0, newArray, 0, size());
        newArray[size()] = element;
        array = newArray;
        return true;
    }

    @Override
    public void clear() {
        array = (E[])new Object[0];
    }

    public void add2(int index, E element) {
        // Both sides of class
        E[] newArray = (E[])new Object[size()+1];
        for (int i = 0; i < index; i++){
            newArray[i] = array[i];
        }
        newArray[index] = element;
        for (int i = index+1; i < size()+1; i++){
            newArray[i] = array[i-1];
        }
        array = newArray;
    }

    @Override
    public void add(int index, E element) {
        // Both sides of class
        if (index<0 || index>size()) {
            throwIndexOutOfBounds(index);
        }
        Object[] newArray = new Object[size() + 1];

        for (int i = 0; i <= size(); i++) {
            if (i < index) {
                newArray[i] = array[i];
            } else if (i == index) {
                newArray[i] = element;
            } else {
                newArray[i] = array[i - 1];
            }
        }
        array = (E[]) newArray;
    }

    @Override
    public boolean contains(Object target) {
        // Right side of class
        return indexOf(target)!=-1;
    }

    @Override
    public E get(int index) {
        // Right side of class
        if (index < 0 || index >= size()) {
            throwIndexOutOfBounds(index);
        }
        return array[index];
    }

    private void throwIndexOutOfBounds(int index) {
        throw new IndexOutOfBoundsException("Index " + index + " on ArrayList of size " + size());
    }

    public void addFirst(E element) {
        E[] newArray = (E[])(new Object[array.length + 1]);
        newArray[0] = element;
        System.arraycopy(array, 0, newArray, 1, array.length);
        array = newArray;
    }

    @Override
    public E set(int index, E element) throws IndexOutOfBoundsException {
        // Right side of class
        if (index < 0 || index >= size()) {
            throwIndexOutOfBounds(index);
        }
        E oldElement = array[index];
        array[index] = element;
        return oldElement;
    }

    @Override
    public E remove(int index) throws IndexOutOfBoundsException {
        if (index < 0 || index >= size()) {
            throwIndexOutOfBounds(index);
        }
        // Left side of class
        E element = array[index];
        E[] newArray = (E[]) new Object[size() - 1];
        System.arraycopy(array, 0, newArray, 0, index);
        System.arraycopy(array, index + 1, newArray, index, size() - index - 1);
        array = newArray;
        return element;
    }

    @Override
    public boolean remove(Object target) {
        // Left side of class
        int index = indexOf(target);
        if (index != -1) {
            remove(index);
        }
        return index != -1;
    }

    @Override
    public int indexOf(Object target) {
        int index = -1;
        for(int i=0; index==-1 && i<size(); i++) {
            if(Objects.equals(target, array[i])) {
                index = i;
            }
        }
        return index;
    }

    @Override
    public Object[] toArray() {
        return Arrays.stream(array, 0, size()).toArray();
    }

    @Override
    public Iterator<E> iterator() {
        return new Itr();
    }

   @Override
    public <T> T[] toArray(T[] a) {
        throw new UnsupportedOperationException("too lazy");
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        throw new UnsupportedOperationException("too lazy");
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        throw new UnsupportedOperationException("too lazy");
    }

    @Override
    public boolean addAll(int index, Collection<? extends E> c) {
        throw new UnsupportedOperationException("too lazy");
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        throw new UnsupportedOperationException("too lazy");
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        throw new UnsupportedOperationException("too lazy");
    }

    @Override
    public int lastIndexOf(Object o) {
        throw new UnsupportedOperationException("too lazy");
    }

    @Override
    public ListIterator<E> listIterator() {
        throw new UnsupportedOperationException("too lazy");
    }

    @Override
    public ListIterator<E> listIterator(int index) {
        throw new UnsupportedOperationException("too lazy");
    }

    @Override
    public List<E> subList(int fromIndex, int toIndex) {
        throw new UnsupportedOperationException("too lazy");
    }

    private class Itr implements Iterator<E> {
        private int position = -1;
        private boolean okayToRemove = false;

        /**
         * Returns {@code true} if the iteration has more elements.
         * (In other words, returns {@code true} if {@link #next} would
         * return an element rather than throwing an exception.)
         *
         * @return {@code true} if the iteration has more elements
         */
        @Override
        public boolean hasNext() {
            return position+1<size();
        }

        /**
         * Returns the next element in the iteration.
         *
         * @return the next element in the iteration
         * @throws NoSuchElementException if the iteration has no more elements
         */
        @Override
        public E next() {
            if(!hasNext()) {
                throw new NoSuchElementException("sorry, I'm all out of elements");
            }
            okayToRemove = true;
            return array[++position];
        }

        /**
         * Removes from the underlying collection the last element returned
         * by this iterator (optional operation).  This method can be called
         * only once per call to {@link #next}.
         * <p>
         * The behavior of an iterator is unspecified if the underlying collection
         * is modified while the iteration is in progress in any way other than by
         * calling this method, unless an overriding class has specified a
         * concurrent modification policy.
         * <p>
         * The behavior of an iterator is unspecified if this method is called
         * after a call to the {@link #forEachRemaining forEachRemaining} method.
         *
         * @throws UnsupportedOperationException if the {@code remove}
         *                                       operation is not supported by this iterator
         * @throws IllegalStateException         if the {@code next} method has not
         *                                       yet been called, or the {@code remove} method has already
         *                                       been called after the last call to the {@code next}
         *                                       method
         * @implSpec The default implementation throws an instance of
         * {@link UnsupportedOperationException} and performs no other action.
         */
        @Override
        public void remove() {
            if(!okayToRemove) {
                throw new IllegalStateException("Must call next() first");
            }
            okayToRemove = false;
            ArrayList.this.remove(position--);
        }
    }
}
